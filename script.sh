#!/usr/bin/env bash

echo "#0 export PAT"

if [[ "$ACTION" == "uninstall all agents" ]]
then
    for agent in $(systemctl list-units --type=service --all | grep -i x2dagent | awk '{print $1}')
    do
        echo "Uninstall $agent from the host"
        AGENT_SERVICE_NAME=$(echo "$agent" | sed 's/\\/\\\\/g')
        AGENT_DIR=$(grep -i "workingdirectory" /etc/systemd/system/$AGENT_SERVICE_NAME | cut -d'=' -f2)
        cd $AGENT_DIR
        sudo ./svc.sh stop
        sudo ./svc.sh uninstall
        if [[ $(sudo ./svc.sh status | grep -i "not installed") ]]
        then
            echo "Uninstall $agent from the pool"
            ./config.sh remove --unattended --auth pat
            rm -r $AGENT_DIR
        else
            echo "Could not uninstall $agent"
        fi
        cd -
    done
    exit 0
fi

AGENTS_INSTALLED="$(systemctl list-units --type=service --all | grep -i x2dagent | wc -l)"

echo "Number of agents already installed on the host: $AGENTS_INSTALLED"
echo "Max number of agents: $AGENTS_MAX_NUMBER"

if [[ $AGENTS_INSTALLED -ge $AGENTS_MAX_NUMBER ]]
then
    echo "The number of agents already installed on the host is greater than or equal to the specified maximum number of agents."
    exit 0
fi

AGENTS_TO_INSTALL=$(($AGENTS_MAX_NUMBER - $AGENTS_INSTALLED))
echo "Number of agents to install: $AGENTS_TO_INSTALL"

echo "#0 download agent tar"
wget --no-verbose --output-file="wget.log" $AGENT_DOWNLOAD_URL || exit 1

TAR_FILE="$(echo $AGENT_DOWNLOAD_URL | awk -F '/' '{print $NF}')"

for i in $(seq 1 $AGENTS_TO_INSTALL)
do
    AGENT_DIR_NAME="agent_$(date +%s)_$i"

    echo "#$i.1 create agent directory"
    mkdir $AGENT_DIR_NAME && cd $AGENT_DIR_NAME || exit 1

    echo "#$i.2 extract agent"
    tar zxf "../$TAR_FILE" -C ./ || exit 1

    echo "#$i.3 configure agent"
    ./config.sh --unattended \
                --acceptTeeEula \
                --url $ORGANIZATION_URL \
                --auth pat \
                --pool $AGENT_POOL_NAME \
                --agent "$AGENT_DIR_NAME"

    echo "#$i.4 install agent as a service"
    sudo ./svc.sh install

    echo "#$i.5 start agent as a service"
    sudo ./svc.sh start

    cd ..
done
rm ./"$TAR_FILE"
